FactoryGirl.define do 
	factory :user do
		name "Dev"
		email "dev@example.com"
		password "foobar"
		password_confirmation "foobar"
	end
end